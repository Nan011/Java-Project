import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.HashMap;
import java.util.ArrayDeque;
import java.util.ArrayList;

public class PinjamBuku {
    public static void main(String args[]) throws IOException {
        BufferedReader reader = new BufferedReader(new
        InputStreamReader(System.in));
        StringTokenizer tokenizer;
        int n = Integer.parseInt(reader.readLine());
        tokenizer = new StringTokenizer(reader.readLine());
        
        ArrayList<String> entry = new ArrayList<String>();
        HashMap<String, Integer> names = new HashMap<String, Integer>();
        HashMap<String, Boolean> names1 = new HashMap<String, Boolean>();
        HashMap<String, ArrayDeque<String>> entries = new HashMap<String, ArrayDeque<String>>();
        
        for (int i = 0; i < n; i++) {
            String book = tokenizer.nextToken();
            entries.put(book, new ArrayDeque<String>());
        }
        int m = Integer.parseInt(reader.readLine());
        tokenizer = new StringTokenizer(reader.readLine());
        for (int i = 0; i < m; i++) {
            String name = tokenizer.nextToken();
            entry.add(name);
            names.put(name, 0);
            names1.put(name, true);
        }
        int q = Integer.parseInt(reader.readLine());
        for (int i = 0; i < q; i++) {
            String commands[] = reader.readLine().split(" ");
            if (commands[0].equals("PINJAM")) {
                String book = commands[1];
                String name = commands[2];
                if (names1.get(name)) {
                    if (entries.get(book).size() == 0) {
                        names.put(name, names.get(name) + 1);
                    }
                    names1.put(name, false);
                    entries.get(book).addLast(name);
                }
    
            }
            if (commands[0].equals("KEMBALIKAN")) {
                String book = commands[1];
                if (entries.get(book).size() == 0) {
                    String name = entries.get(book).pollFirst();
                    names1.put(name, true);
                    if (entries.get(book).size() > 0) {
                        name = entries.get(book).getFirst();
                        names.put(name, names.get(name) + 1);
                    }
                }
            }
        }
        for (int i = 0; i < entry.size()-1; i++) {
            System.out.print(names.get(entry.get(i)) + " ");
        }
        System.out.println((names.get(entry.get(entry.size() - 1))));
    }
}
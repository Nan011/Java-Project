/**
* Task          : TP1
* Nama          : Nandhika Prayoga
* NPM           : 1706039912
* Kelas         : SDA-C
* Kode Asdos    : C04
* @author   Nandhika Prayoga
*/


import java.util.Scanner;
import java.util.ArrayDeque;

/**
* Class represents problem solver to TP1 case.
*/
public class SDA18191T {
    private static int MODULO = 1000000007; // 
   
    /**
    * This static method handles main system, like
    * handle A algorithm and B algorithm
    * A Algorithm is counting how many ways to get certain number
    * B Algorithm is which shortest path to get certain number in first lexicography
    *
    * @param  args  Arguments
    * @return       Nothing to be returned
    */
    public static void main(String args[]) { // Function
        Scanner scan = new Scanner(System.in);
        String[] input = scan.nextLine().split(" ");
        int distance = Integer.parseInt(input[0]);
        int n = Integer.parseInt(input[1]);
        int milyaSpeed = Integer.parseInt(input[2]);
        int[] dylanSpeeds = new int[n];
        input = scan.nextLine().split(" ");
        
        for (int i = 0; i < input.length; i++) {
            dylanSpeeds[i] = Integer.parseInt(input[i]);
        }
        
        String type = scan.nextLine().toUpperCase();
        if ("A".equals(type)) {
            System.out.println(countWays(dylanSpeeds, milyaSpeed, distance));
        } else if ("B".equals(type)) {
            ArrayDeque<Integer> path = findMinimumPath(dylanSpeeds, milyaSpeed, distance, dylanSpeeds.length-1, 0);
            if (path == null) {
                System.out.println("NA");
            } else {
                System.out.println(path.size());
                for (int i = path.size() - 1; i > 0; i--) {
                    System.out.print(path.pollFirst() + " ");
                }
                System.out.println(path.pollFirst());
            }
        }
    }

    /**
    * This method can counting the number of ways 
    * to meet Dylan and Milya in same point within the distance. 
    * This is A's algorithm.
    *
    * @param dylanSpeeds    Velocity's data in meter.
    * @param milyaSpeed     Milya's velocity in meter.
    * @param distance       Distance between Dylan and Milya in meter.
    * @return               The number of ways to meet Dylan and Milya in same point. 
    */
    private static long countWays(int[] dylanSpeeds, int milyaSpeed, int distance) {
        long[] paths = new long[distance+1];
        return countWays(dylanSpeeds, milyaSpeed, distance, paths, 0);
    }


    /**
    * This method can counting the number of ways 
    * to meet Dylan and Milya in same point within the distance. 
    * This is A's algorithm.
    *
    * @param dylanSpeeds    Velocity's data in meter.
    * @param milyaSpeed     Milya's velocity in meter.
    * @param Distance       Distance between Dylan and Milya in meter.
    * @param paths          List of distance from 0 until distance of Dylan and Milya in meter.
    * @param index          Index of certain distance.
    * @return               The number of ways to meet Dylan and Milya in same point. 
    */
    private static long countWays(int[] dylanSpeeds, int milyaSpeed, int distance, long[] paths, int index) {
        paths[index] = 0;
        for (int i = 0; i < dylanSpeeds.length; i++) {
            int index_before = index - (dylanSpeeds[i] + milyaSpeed);
            if (index_before == 0) {
                paths[index] += 1;
            } else if (index_before > 0) {
                paths[index] += paths[index_before] % MODULO;
            }
        }
        paths[index] = paths[index] % MODULO;

        if (index == distance) {
            return paths[index];
        } else {
            return countWays(dylanSpeeds, milyaSpeed, distance, paths, index + 1);
        }
    }

    /**
    * This method can finding shortest path 
    * to make Dylan and Milya meet to each other.
    * This is B's algorithm.
    *
    * @param dylanSpeeds    Velocity's data in meter.
    * @param milyaSpeed     Milya's velocity in meter.
    * @param distance       Distance between Dylan and Milya in meter.
    * @param index          Index of certain speed in dylanSpeeds.
    * @param val            Total distance in certain path.
    * @return               A list contains some velocity to make Dylan and Milya meet to each other or none. 
    */
    private static ArrayDeque<Integer> findMinimumPath(int[] dylanSpeeds, int milyaSpeed, int distance, int index, int val) {
        int t = -1;
        ArrayDeque<Integer> bestPath = null;    
        for (int i = index; i >= 0; i--) {
            int newVal = val + dylanSpeeds[i] + milyaSpeed;
            if (newVal == distance) {
                bestPath = new ArrayDeque<Integer>();
                bestPath.add(dylanSpeeds[i]);
                return bestPath;
            } else if (newVal < distance) {
                bestPath = findMinimumPath(dylanSpeeds, milyaSpeed, distance, i, newVal);
                if (bestPath != null) {
                    bestPath.add(dylanSpeeds[i]);
                    return bestPath;
                }
            }
        }
        return bestPath;
    }
}
public class SearchingAlgorithm {
    public static void main(String args[]) {
        int arr[] = {1, 1, 1, 3, 4, 8, 11};
		int k = 0;
		int v = -2;
        System.out.println(interpolationSearch(arr, k, arr[k] + v));
    }
    public static int interpolationSearch(int arr[], int elem) {
        int i = 0;
        int j = arr.length - 1;
        while (i < j && elem >= arr[i] && elem <= arr[j]) {
			int newPos = (int) (i + (j - i) * ((double)(elem - arr[i])/(arr[j] - arr[i])));
            if (arr[newPos] == elem) {
				return newPos;
			} else if (arr[newPos] > elem) {
				j = newPos - 1;
			} else {
				i = newPos + 1;
			}
        }
		return (-1);
    }
	
	public static int interpolationSearch(int arr[], int lo, int elem, int a) {
        int i = lo;
        int j = arr.length - 1;
		System.out.println("masuk pak eko");
        while (i < j && elem <= arr[j]) {
			int newPos = (int) (i + (j - i) * ((double)(elem - arr[i])/(arr[j] - arr[i])));
			if (arr[newPos] >= elem) {
				return newPos;
			} else {
				i = newPos + 1;
			}
        }
		return (-1);
    }
	
	public static int interpolationSearch(int arr[], int lo, long elem) {
		int i = lo;
		int j = arr.length - 1;
		if (elem < arr[i] || elem > arr[j]) {
			return (-1);
		} else {
			int newPos = 0;
			while (i < j && elem >= arr[i] && elem <= arr[j]) {
				newPos = (int) (i + (j - i) * ((double)(elem - arr[i])/(arr[j] - arr[i])));
				if (arr[newPos] == elem) {
					return newPos;
				} else if (arr[newPos] > elem) {
					j = newPos - 1;
				} else {
					i = newPos + 1;
				}
			}
			return newPos + 1;
		}
	}
}

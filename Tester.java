public class Tester {
	public static void main(String[] args) {
		
	}

	public Matrix multiply(Matrix other) {
		Matrix result;
		result = new Matrix(this.totalRow, other.totalColumn);
		for (int r = 0; r < this.totalRow; r++) {
			for (int c = 0; c < other.totalColumn; c++) {
				int sum = 0;
				for (int i = 0; i < this.totalColumn; i++) {
					sum += this.matrix[i][r] * other.matrix[c][i];
				}
				result.set(r, c, sum);
			}
		}
		return(result);
	}
}
import java.util.Scanner;

public class Minesweeper {
	private int[][] field;
	
	public Minesweeper(int height, int width) {
		this.field = new int[height][width];
		for (int row = 0; row < this.field.length; row++) {
			for (int col = 0; col < this.field[0].length; col++) {
				this.field[row][col] = 0;
			}
		}
	}
	
	public void setMines(int row, int col) {
		this.field[row][col] = -1;
		boolean up = true, right = true, down = true, left = true;
		if (row - 1 < 0) up = false;
		if (col + 1 >= this.field[0].length) right = false;
		if (row + 1 >= this.field.length) down = false;
		if (col - 1 < 0) left = false;
		
		changeStatus(up, row - 1, col);
		changeStatus(right, row, col + 1);
		changeStatus(down, row + 1, col);
		changeStatus(left, row, col-1);
		
		changeStatus(up, right, row - 1, col + 1);
		changeStatus(right, down, row + 1, col + 1);
		changeStatus(down, left, row + 1, col - 1);
		changeStatus(left, up, row - 1, col - 1);
	}
	
	private void changeStatus(boolean outgrid, int row, int col) {
		if (outgrid && this.field[row][col] != -1) this.field[row][col] += 1;
	}
	
	private void changeStatus(boolean outgrid1, boolean outgrid2, int row, int col) {
		if (outgrid1 && outgrid2 && this.field[row][col] != -1) this.field[row][col] += 1;
	}
	
	public String toString() {
		String info = "";
		for (int row = 0; row < this.field.length; row++) {
			for (int col = 0; col < this.field[0].length; col++) {
				if (this.field[row][col] == -1) info += "*"; 
				else info += this.field[row][col];
			}
			info += "\n";
		}
		return info;
	}
	
	public static void main(String[] args) {
		Minesweeper game1 = createMinesweeper();
		Minesweeper game2 = createMinesweeper();
		System.out.print("Field #1:\n" + game1 + "\n");
		System.out.println("Field #2:\n" + game2);
	}
	
	public static Minesweeper createMinesweeper() {
		int m, n;
		Scanner scan = new Scanner(System.in);
		String[] input = scan.nextLine().split(" ");
		m = Integer.parseInt(input[0]);
		n = Integer.parseInt(input[1]);
		Minesweeper game = new Minesweeper(m, n);
		for (int i = 0; i < m; i++) {
			String[] row = scan.nextLine().split("");
			for (int j = 0; j < n; j++) {
				if (row[j].equals("*")) {
					game.setMines(i, j);
				}
			}
		}
		return game;
	}
}

public class Sorting {
	private static void quickSort3(int[] arr) {
        quickSort3(arr, 0, arr.length-1);
        return ;
    }
   
    private static void quickSort3(int[] arr, int i, int j) {
        if (i > j) {
            return ;
        } else {
            int l = i;
            int r = j;
            boolean swi = true;
            while (l < r) {
                if (arr[l] > arr[r]) {
                    arr[l] += arr[r];
                    arr[r] = arr[l] - arr[r];
                    arr[l] -= arr[r];
                    swi = !swi;
                }
                if (swi) l++;
                else r--;
                
            }
            quickSort3(arr, i, l-1);
            quickSort3(arr, r+1, j);
        }
    }
}